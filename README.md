# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062226
- [ ] Name :陳評詳	
- [ ] URL :https://startbootstrap.com/template-overviews/clean-blog/

## 10 Bootstrap elements (list in here)
1. 將"A Blog Theme by Start Bootstrap"用small tag 變小
2. 用class-container將想顯示的內容包起來使其變成responsive
3. 用class-row來做排版
4. navbar-collapse	來隱藏 並取代 a menu/hamburger icon on mobile phones and small tablets
5. 用list-inline-item來讓想顯示的ite集中於同一行
6. 用navlink 來讓navbar裡的東西向超連結
7. 用masthead產生標題
8. 用subhead產生副標題
9. 用fa fa-bars產生一個bar
10.用nav-itwm指地想要變更的navigation item